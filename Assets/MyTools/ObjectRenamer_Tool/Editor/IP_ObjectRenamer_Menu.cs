using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace IndiePixel.Tools
{
    public class IP_ObjectRenamer_Menu
    {
        [MenuItem("MyTools/ObjectRenameTool")]
        public static void ObjectRename()
        {
            IP_ObjectRenamer_Editor.LaunchEditor();
        }
    }
}

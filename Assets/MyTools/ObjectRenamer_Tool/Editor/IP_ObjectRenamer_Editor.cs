using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace IndiePixel.Tools
{
    public class IP_ObjectRenamer_Editor : EditorWindow
    {
        #region Variables
        /// <summary>
        /// 多選物件陣列
        /// </summary>
        GameObject[] selectedObjects = new GameObject[0];

        /// <summary>
        /// 前綴
        /// </summary>
        string wantedPrefix;

        /// <summary>
        /// 名稱
        /// </summary>
        string wantedName;

        /// <summary>
        /// 後綴
        /// </summary>
        string wantedSuffix;

        /// <summary>
        /// 是否添加尾數
        /// </summary>
        bool addNumbering;
        #endregion

        #region BuiltIn Methods
        public static void LaunchEditor()
        {
            var win = GetWindow<IP_ObjectRenamer_Editor>("Object Rename");
            win.Show();
        }

        private void OnGUI()
        {
            //取得目前多選
            selectedObjects = Selection.gameObjects;

            //顯示目前多選數量
            EditorGUILayout.LabelField("Selection Count: " + selectedObjects.Length.ToString("000"));

            //最外層直向開始
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space(10);

            //直向第二層
            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space(10);

            //直向第一層
            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space(10);

            wantedPrefix = EditorGUILayout.TextField("Prefix: ", wantedPrefix, EditorStyles.miniTextField, GUILayout.ExpandWidth(true));
            wantedName = EditorGUILayout.TextField("Name: ", wantedName, EditorStyles.miniTextField, GUILayout.ExpandWidth(true));
            wantedSuffix = EditorGUILayout.TextField("Suffix: ", wantedSuffix, EditorStyles.miniTextField, GUILayout.ExpandWidth(true));
            addNumbering = EditorGUILayout.Toggle("AddNumbering? ", addNumbering);

            //最外層直向結束
            EditorGUILayout.Space(10);
            EditorGUILayout.EndVertical();

            if (GUILayout.Button("Rename Selected Objects", GUILayout.Height(45), GUILayout.ExpandWidth(true)))
            {
                RenameObjects();
            }

            //直向第二層結束
            EditorGUILayout.Space(10);
            EditorGUILayout.EndVertical();

            //最外層直向結束
            EditorGUILayout.Space(10);
            EditorGUILayout.EndHorizontal();

            Repaint();
        }
        #endregion

        #region Custom Methods
        void RenameObjects()
        {
            Array.Sort(selectedObjects, delegate (GameObject objA, GameObject objB) { return objA.name.CompareTo(objB.name); });

            for (int i = 0; i < selectedObjects.Length; i++)
            {
                string fileName = string.Empty;

                //前綴
                if (wantedPrefix.Length > 0)
                {
                    fileName += wantedPrefix;
                }

                //名稱
                if (wantedName.Length > 0)
                {
                    fileName += "_" + wantedName;
                }

                //前綴
                if (wantedSuffix.Length > 0)
                {
                    fileName += "_" + wantedSuffix;
                }

                //前綴
                if (addNumbering)
                {
                    fileName += "_" + i.ToString("000");
                }

                selectedObjects[i].name = fileName;
            }
        }
        #endregion
    }
}

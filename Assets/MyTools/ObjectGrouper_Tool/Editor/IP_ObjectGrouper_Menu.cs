using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace IndiePixel.Tools
{
    public class IP_ObjectGrouper_Menu
    {
        [MenuItem("MyTools/ObjectGroupTool %#g")]   //%#g:可使用Shift開啟
        public static void ObjectGroup()
        {
            IP_ObjectGrouper_Editor.LaunchEditor();
        }

    }
}

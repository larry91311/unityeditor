using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace IndiePixel.Tools
{
    public class IP_ProjectSetup_Menu
    {
        [MenuItem("MyTools/ProjectSetupTool")]
        public static void InitProjectSetup()
        {
            IP_ProjectSetup_Editor.LaunchEditor();
        }
    }
}

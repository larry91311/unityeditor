using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace IndiePixel.Tools
{
    public class IP_ObjectReplacer_Menu
    {
        [MenuItem("MyTools/ObjectReplaceTool")]
        public static void ObjectReplace()
        {
            IP_ObjectReplacer_Editor.LaunchEditor();
        }
    }
}

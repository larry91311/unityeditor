using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace IndiePixel.Tools
{
    public class IP_ProjectSetup_Editor : EditorWindow
    {
        #region Variables
        /// <summary>
        /// 視窗-關閉檢測可能影響Repaint檢測故存
        /// </summary>
        static IP_ProjectSetup_Editor win;

        /// <summary>
        /// 遊戲名稱-資料夾主體名稱
        /// </summary>
        private string gameName;
        #endregion

        #region BuiltIn Methods
        public static void LaunchEditor()
        {
            win = GetWindow<IP_ProjectSetup_Editor>("Project Setup");
            win.Show();
        }

        private void OnGUI()
        {
            EditorGUILayout.BeginHorizontal();

            //最上層資料夾主體名稱輸入框
            gameName = EditorGUILayout.TextField("Game Name: ", gameName);

            EditorGUILayout.EndHorizontal();

            //執行按鈕
            if (GUILayout.Button("Create Project Structure", GUILayout.Height(35), GUILayout.ExpandWidth(true)))
            {
                CreateProjectFolders();
            }

            if (win != null)
            {
                Repaint();
            }
        }
        #endregion

        #region Custom Methods
        void CreateProjectFolders()
        {
            //gameName檢測
            if (string.IsNullOrEmpty(gameName))
            {
                return;
            }
            if (gameName == "Game")
            {
                if (!EditorUtility.DisplayDialog("Project Setup Warning", "Do you really want to call your project Game?", "Yes", "No"))
                {
                    //No選項
                    CloseWindow();
                    return;
                }
            }

            //取得專案Asset路徑
            string assetPath = Application.dataPath;

            //建立新資料夾最上層-路徑+建立
            string rootPath = assetPath + "/" + gameName;
            DirectoryInfo rootInfo = Directory.CreateDirectory(rootPath);

            if (!rootInfo.Exists)
            {
                return;
            }
            //建立第二三層資料夾
            CreateSubFolders(rootPath);

            //刷新Asset頁面
            AssetDatabase.Refresh();

            //刷新完關閉視窗
            CloseWindow();
        }

        //建立第二三層資料夾
        void CreateSubFolders(string aRootFolder)
        {
            //新增子資料夾資訊
            DirectoryInfo rootInfo = null;

            List<string> afolderList = new List<string>();

            //新增資料夾
            rootInfo = Directory.CreateDirectory(aRootFolder + "/" + "Art");
            if (rootInfo.Exists)
            {
                afolderList.Clear();
                afolderList.Add("Animation");
                afolderList.Add("Audio");
                afolderList.Add("Fonts");
                afolderList.Add("Materials");
                afolderList.Add("Objects");
                afolderList.Add("Textures");

                CreateFolders(aRootFolder + "/" + "Art", afolderList);
            }

            //新增資料夾
            rootInfo = Directory.CreateDirectory(aRootFolder + "/" + "Code");
            if (rootInfo.Exists)
            {
                afolderList.Clear();
                afolderList.Add("Editor");
                afolderList.Add("Scripts");
                afolderList.Add("Shaders");

                CreateFolders(aRootFolder + "/" + "Code", afolderList);
            }

            //新增資料夾
            rootInfo = Directory.CreateDirectory(aRootFolder + "/" + "Scene");
            if (rootInfo.Exists)
            {
                CreateScene(aRootFolder + "/" + "Scene", "_Main");
            }
        }

        //依據List創立資料夾
        void CreateFolders(string aPath, List<string> folders)
        {
            foreach (string folder in folders)
            {
                Directory.CreateDirectory(aPath + "/" + folder);
            }
        }

        void CreateScene(string aPath, string aName)
        {
            Scene curScene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Single);
            EditorSceneManager.SaveScene(curScene, aPath + "/" + aName + ".unity", true);
        }

        void CloseWindow()
        {
            if (win)
            {
                win.Close();
            }
        }
        #endregion
    }
}

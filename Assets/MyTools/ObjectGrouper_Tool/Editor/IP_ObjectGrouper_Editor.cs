using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace IndiePixel.Tools
{
    public class IP_ObjectGrouper_Editor : EditorWindow
    {
        #region Variables
        /// <summary>
        /// 群組名稱
        /// </summary>
        string wantedName;

        /// <summary>
        /// 目前多選數量
        /// </summary>
        int currentSelectionCount = 0;

        /// <summary>
        /// 多選物件
        /// </summary>
        GameObject[] selected = new GameObject[0];
        #endregion

        #region BuiltIn Methods
        public static void LaunchEditor()
        {
            var win = GetWindow<IP_ObjectGrouper_Editor>("Object Group");
            win.Show();
        }

        private void OnGUI()
        {
            //多選數量等同目前選擇物件陣列長度
            selected = Selection.gameObjects;
            currentSelectionCount = selected.Length;

            //最外層直向開始
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space();

            //直向第一層
            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Selection Count: " + currentSelectionCount.ToString());

            GUILayout.Space(5);

            EditorGUILayout.LabelField("Group Name", EditorStyles.boldLabel);
            wantedName = EditorGUILayout.TextField(wantedName);

            if (GUILayout.Button("Group Selected", GUILayout.Height(45), GUILayout.ExpandWidth(true)))
            {
                GroupSelectedObjects();
            }

            //最外層直向結束
            EditorGUILayout.Space();
            EditorGUILayout.EndVertical();

            //最外層直向結束
            EditorGUILayout.Space();
            EditorGUILayout.EndHorizontal();

            Repaint();
        }
        #endregion

        #region Custom Methods
        void GroupSelectedObjects()
        {
            if (selected.Length > 0)
            {
                if (wantedName != "Enter Name")
                {
                    GameObject groupGO = new GameObject(wantedName + "_GRP");

                    foreach (GameObject curgo in selected)
                    {
                        curgo.transform.SetParent(groupGO.transform);
                    }
                }
                else
                {
                    EditorUtility.DisplayDialog("Group Message", "You must provide a name for your group!", "OK");
                }
            }
        }
        #endregion
    }
}

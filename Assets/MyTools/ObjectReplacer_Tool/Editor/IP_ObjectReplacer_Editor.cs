using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace IndiePixel.Tools
{
    public class IP_ObjectReplacer_Editor : EditorWindow
    {
        #region Variables
        /// <summary>
        /// 目前多選數量
        /// </summary>
        int currentSelectionCount = 0;

        /// <summary>
        /// 要替換成的物件
        /// </summary>
        GameObject wantedObject = null;
        #endregion

        #region BuiltIn Methods
        public static void LaunchEditor()
        {
            var win = GetWindow<IP_ObjectReplacer_Editor>("Object Replace");
            win.Show();
        }

        private void OnGUI()
        {
            //取得多點選數量
            GetSelections();

            //最外層直向開始
            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space();

            //顯示多點選數量
            EditorGUILayout.LabelField("Selection Count: " + currentSelectionCount.ToString(), EditorStyles.boldLabel);

            EditorGUILayout.Space();

            wantedObject = (GameObject)EditorGUILayout.ObjectField("Replace Object: ", wantedObject, typeof(GameObject), true);
            if (GUILayout.Button("Replace Selected Objects", GUILayout.ExpandWidth(true), GUILayout.Height(48)))
            {
                ReplaceSelectedObjects();
            }
            //最外層直向結束
            EditorGUILayout.Space();
            EditorGUILayout.EndVertical();

            Repaint();
        }
        #endregion

        #region Custom Methods
        void GetSelections()
        {
            //重置多選數量
            currentSelectionCount = 0;

            //多選數量等同目前選擇物件陣列長度
            currentSelectionCount = Selection.gameObjects.Length;
        }

        void ReplaceSelectedObjects()
        {
            //目前選擇數量 不可為0
            if (currentSelectionCount == 0)
            {
                CustomDialog("At least one Object must be selected");
                return;
            }

            //目前設定物件 不可為空
            if (!wantedObject)
            {
                CustomDialog("At least one wantedObject must be set");
                return;
            }

            //替換物件
            GameObject[] selectedObjects = Selection.gameObjects;
            for (int i = 0; i < selectedObjects.Length; i++)
            {
                Transform selectTransform = selectedObjects[i].transform;
                GameObject newObject = Instantiate(wantedObject, selectTransform.position, selectTransform.rotation);
                newObject.transform.localScale = selectTransform.localScale;

                DestroyImmediate(selectedObjects[i]);
            }
        }

        void CustomDialog(string aMessage)
        {
            EditorUtility.DisplayDialog("Replace Objects Warning", aMessage, "YES", "NO");
        }
        #endregion
    }
}
